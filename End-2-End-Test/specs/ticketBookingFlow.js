const jsonfile = require('jsonfile');
const path = require('path');
const scriptName = path.basename(__filename);

var quotation;
var paymetsAndProducts;
var travelInformation;
var paymentInformation;
var confirmation;
let testdata = jsonfile.readFileSync(__dirname+'/../test-data/'+ scriptName+'on');
module.exports = 
{
  '@tags': ['e2e'],
  before: function (browser) {
    browser.page.quotation().navigate();
    quotation=browser.page.quotation();
    paymetsAndProducts=browser.page.paymetsAndProducts();
    travelInformation=browser.page.travelInformation();
    paymentInformation=browser.page.paymentInformation();
    confirmation=browser.page.confirmation();
    browser.frame(0)
    browser.useXpath()
  },
  'End-to-End Flow Of Ticket Booking': function (browser) 
  {
    /**
     * Step 1 Quotation Page validation and enterinfg the Test Data
    */
    quotation.validateQuotationPage()
    quotation.selectTypeOfCover(testdata.Tc_Id1.selectTypeOfCover)
    quotation.selectmultiDestination(testdata.Tc_Id1.selectmultiDestination)
    quotation.selectFromDate(testdata.Tc_Id1.selectFromDate)
    quotation.selectToDate(testdata.Tc_Id1.selectToDate)
    quotation.selectNumberOfPerson(testdata.Tc_Id1.selectNumberOfPerson)
    quotation.fillTravellerDetails(testdata.Tc_Id1.fillTravellersAge)
    quotation.applyPromoCode(testdata.Tc_Id1.applyPromoCode)
    quotation.clickSubmit()
    /**
     * Step 2 Selecting the Products
     */
    paymetsAndProducts.validatePaymentsAndProductsPage()
    paymetsAndProducts.selectProduct(testdata.Tc_Id1.selectProduct)
    /**
     * Step 3 Providing the Traveller Information
     */
    travelInformation.validateTravelInformationPage();
    travelInformation.fillTravellerDetails(testdata.Tc_Id1.fillTravellerDetails)
    travelInformation.fillSubscriberDetailsAndSubmit(testdata.Tc_Id1.addressDetails)
    /**
    * Step 4 Payment Information
    */
    paymentInformation.validatePaymentInformationPage()
    paymentInformation.clickSubmit()


    /**
     * Step 5 Confirmation
     */
    confirmation.validateConfirmationPage()
    confirmation.fillCardDetailsAndSubmit(testdata.Tc_Id1.paymentDetails)
  
  //Did not add the ticket confirmation page validatoin (whch was shown after the ticket booking as the flow ends up with payment failure error)

  },
  after: function (browser) 
  {
    browser
    .pause(5000)
    .end();
  }
};