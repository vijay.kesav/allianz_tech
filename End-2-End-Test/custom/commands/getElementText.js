exports.command = function (selector, callback) {
  var self = this;

  this.execute(
    function (css) {
      let text = document.querySelectorAll(css).textContent;
      return text;
    },

    [selector],

    function (result) {
      if (typeof callback === "function") {
        callback.call(self, result);
      }
    }

  );
  return this;
};
