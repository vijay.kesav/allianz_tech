let dynamicXpath=
{
    typeOfCoverValue:'//span[text()=\'value\']',
    yearValue:'//span[@class=\'year\'][text()=\'value\']',
    monthValue:'//div[@class=\'datepicker-months\']/table/tbody/tr/td/span[text()=\'value\']',
    dayValue:'//div[@class=\'datepicker-days\']/table/tbody/tr/td[@class=\'day\'][text()=\'value\']',
    personValue:'//li//span[contains(text(),\'value  person\')]',
    multiDestinationValue:'//span[text()=\'value\']',
    multiDestinationTextBox:'//button[@data-id=\'multiDestination\']',
    ageValue:'//input[@name=\'ageAtDeparturevalue\']',
    pricePerperson:'//input[@name=\'pricePerPersonvalue\']'
}

module.exports = 
{
    url: function () 
    {
      const globals = this.api.globals
      return globals.devServerURL
    },
    elements: 
    {
      menuTitle: 
      {
        locateStrategy: 'xpath',
        selector: '//span[@class=\'menu ng-binding\'][text()=\'Quotation\']'
        
      },
      typeOfCoverLabel:
      {
        locateStrategy: 'xpath',
        selector:'//label[@for=\'typeOfCover\']'
      },
      typeOfCoverValue:
      {
        locateStrategy: 'xpath',
        selector:'//button[@data-id=\'typeOfCover\']'
      },
      multiDestinationValue:
      {
        locateStrategy:'xpath',
        selector:'//button[@data-id=\'multiDestination\']'
      },

      yourDepatureDatesLabel:
      {
        locateStrategy:'xpath',
        selector:'//div[@class=\'travelerLabel\']/label[text()=\'Your departure dates\']'
      },
      yourDepatureDatesFromValue:
      {
        locateStrategy:'xpath',
        selector:'//div[contains(@ng-click,\'travelDatesFrom\')]'
      },
      yourDepatureDatesToValue:
      {
        locateStrategy:'xpath',
        selector:'//div[contains(@ng-click,\'travelDatesTo\')]'
      },
      datepickerSwitchDays:
      {
        locateStrategy:'xpath',
        selector:'//div[@class=\'datepicker-days\']/table//th[@class=\'datepicker-switch\']'
      },
      datepickerSwitchMonths:
      {
        locateStrategy:'xpath',
        selector:'//div[@class=\'datepicker-months\']/table//th[@class=\'datepicker-switch\']'
      },
      datepickerForwardBtn:
      {
        locateStrategy:'xpath',
        selector:'//div[@class=\'datepicker-years\']//table//th[@class=\'next\']'
      },
      numberOfPersonLabel:
      {
        locateStrategy:'xpath',
        selector:'//label[@for=\'numberOfPerson\']'
      },
      numberOfPersonValue:
      {
        locateStrategy:'xpath',
        selector:'//button[@data-id=\'numberOfPerson\']'
      },
      promotionCodeLabel:
      {
        locateStrategy:'xpath',
        selector:'//label[@for=\'promotionCode\']'
      },
      promotionCodeValue:
      {
        locateStrategy:'xpath',
        selector:'//input[@name=\'promotionCode\']'
      },
      submitBtn:
      {
        locateStrategy:'xpath',
        selector:'//button[@id=\'submit\']'
      }
    },
    commands: [{
    validateQuotationPage: function () 
    {
        this
        .waitForElementVisible('@menuTitle', timeOut)
        .waitForElementVisible('@typeOfCoverLabel',timeOut)
        .waitForElementVisible('@yourDepatureDatesLabel',timeOut)
        .waitForElementVisible('@numberOfPersonLabel',timeOut)
        .waitForElementVisible('@promotionCodeLabel',timeOut)
        return this;
    },
    
    selectTypeOfCover: function(cover)
    {
        this
        .waitForElementVisible('@typeOfCoverValue', timeOut)
        .click('@typeOfCoverValue')
        .api.useXpath()
        .click(dynamicXpath.typeOfCoverValue.replace('value',cover))
        return this;
    },
    selectmultiDestination: function(Destinations)
    {
        this
        .waitForElementVisible('@multiDestinationValue',timeOut)
        .click('@multiDestinationValue')
        for(var i=0;i<Destinations.length;i++)
        {
            this.api.useXpath()
            .click(dynamicXpath.multiDestinationValue.replace('value',Destinations[i]));
        }
            this.click(dynamicXpath.multiDestinationTextBox);
        return this;
    },
    selectFromDate: function(date)
    {
        var dates = date.split("-");
        this
        .waitForElementVisible('@yourDepatureDatesFromValue', timeOut)
        .click('@yourDepatureDatesFromValue')
        .click('@datepickerSwitchDays')
        .click('@datepickerSwitchMonths')
        .click('@datepickerForwardBtn')
        .api.useXpath()
        .waitForElementVisible(dynamicXpath.yearValue.replace('value',dates[2]),timeOut)
        .click(dynamicXpath.yearValue.replace('value',dates[2]))
        .click(dynamicXpath.monthValue.replace('value',dates[1]))
        .click(dynamicXpath.dayValue.replace('value',dates[0]));
        return this;
    },
    selectToDate: function(userdate)
    {
        var dates = userdate.split("-");
        this
        .waitForElementVisible('@yourDepatureDatesToValue',timeOut)
        .click('@yourDepatureDatesToValue')
        .click('@datepickerSwitchDays')
        .click('@datepickerSwitchMonths')
        .click('@datepickerForwardBtn')
        .api.useXpath()
        .waitForElementVisible(dynamicXpath.yearValue.replace('value',dates[2]),timeOut)
        .click(dynamicXpath.yearValue.replace('value',dates[2]))
        .click(dynamicXpath.monthValue.replace('value',dates[1]))
        .click(dynamicXpath.dayValue.replace('value',dates[0]));
        return this;
    },
    selectNumberOfPerson: function(count)
    {
        this
        .waitForElementVisible('@numberOfPersonValue',timeOut)
        .click('@numberOfPersonValue')
        .api.useXpath()
        .click(dynamicXpath.personValue.replace('value',count));
        return this;
    },
    fillTravellerDetails: function(details)
    {
        for(var i=0;i<details.length;i++)
        {
            this.api.useXpath()
            .waitForElementVisible(dynamicXpath.ageValue.replace('value',i+1),timeOut)
            .setValue(dynamicXpath.ageValue.replace('value',i+1),details[i][0])
            .setValue(dynamicXpath.pricePerperson.replace('value',i+1),details[i][1]);
        }
        return this;
    },
    applyPromoCode: function(promoCode)
    {
        this
        .waitForElementVisible('@promotionCodeValue',timeOut)
        .setValue('@promotionCodeValue',promoCode)
        return this;
    },
    clickSubmit: function()
    {
        this
        .waitForElementVisible('@submitBtn', timeOut)
        .click('@submitBtn')
        return this;
    }

    }]
}