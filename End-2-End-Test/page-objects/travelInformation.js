
let dynamicXpath=
{
   fsNameValue:'//input[@name=\'firstNamevalue\']',
   lsNameValule:'//input[@name=\'lastNamevalue\']',
   dob:'//input[@name=\'dateOfBirth1\']',
   personalId:'//button[@data-id=\'personalIDType1\']',
   personalIdValue:'//div[contains(@class,\'personalIDType1\')]//li//span[contains(text(),\'value\')]',
   personalIdNumber:'//input[@id=\'personalIDNumbervalue\']'
}

module.exports = 
{
    elements: 
    {
      commonNo:
      {
        locateStrategy: 'xpath',
        selector:'//span[contains(@ng-bind-html,\'common.no\')]'
      },
      firstName:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingFirstName\']'
      },
      lastName:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingLastName\']'
      },
      passportNumber:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingPassportNumber\']'
      },
      dob:{
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingDateOfBirth\']'
      },
      address:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingAddress\']'
      },
      zipCode:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingZipCode\']'
      },
      city:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingCity\']'
      },
      province:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingProvince\']'
      },
      phoneNumber:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingPhoneNumber\']'
      },
      email:
      {
        locateStrategy: 'xpath',
        selector:'//input[@id=\'billingEmail\']'
      },
      confirmDisclaimer:
      {
        locateStrategy: 'xpath',
        selector:'//label[@for=\'confirmDisclaimer\']'
      },
      submitBtn:
      {
        locateStrategy:'xpath',
        selector:'//button[@id=\'submit\']'
      }
    
    },
    commands: [{
    validateTravelInformationPage: function () 
    {
        this
        .api.useXpath()
        .waitForElementNotPresent(spinner, timeOut)
        .waitForElementVisible(currentMenu, timeOut)
        .expect.element(currentMenu).text.to.contain(stepThreeTitle);
        return this;
      },
    fillTravellerDetails: function(details)
    {
        for(var i=0;i<details.length;i++)
        {
            this.api.useXpath()
            .waitForElementVisible(dynamicXpath.fsNameValue.replace('value',i+1),timeOut)
            .setValue(dynamicXpath.fsNameValue.replace('value',i+1),details[i][0])
            .setValue(dynamicXpath.lsNameValule.replace('value',i+1),details[i][1])
            .setValue(dynamicXpath.dob.replace('value',i+1),details[i][2])
            .click(dynamicXpath.personalId.replace('value',i+1))
            .waitForElementVisible(dynamicXpath.personalIdValue.replace('value',details[i][3]),timeOut)
            .click(dynamicXpath.personalIdValue.replace('value',details[i][3]))
            .setValue(dynamicXpath.personalIdNumber.replace('value',i+1),details[i][4]);
        }
        return this; 
    },
    fillSubscriberDetailsAndSubmit: function(details)
    {
        this
        .waitForElementVisible('@firstName',timeOut)
        .click('@commonNo')
        .setValue('@firstName',details.firstName)
        .setValue('@lastName',details.lastName)
        .setValue('@passportNumber',details.passportNumber)
        .click('@dob')
        .setValue('@dob',details.dob)
        .setValue('@address',details.address)
        .setValue('@zipCode',details.zipCode)
        .setValue('@city',details.city)
        .setValue('@province',details.province)
        .setValue('@phoneNumber',details.phoneNumber)
        .setValue('@email',details.email)
        .click('@confirmDisclaimer')
        .click('@submitBtn')
        return this;
    },
    }] 
}