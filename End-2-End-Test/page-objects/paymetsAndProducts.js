module.exports = 
{
    elements: 
    {
      menuTitle: 
      {
        selector: '.select-button'
      }
    },
    commands: [{
    validatePaymentsAndProductsPage: function () 
    {
        this
        .api.useXpath()
        .waitForElementNotPresent(spinner, timeOut)
        .waitForElementVisible(currentMenu, timeOut)
        .expect.element(currentMenu).text.to.contain(stepTwoTitle);
        return this;
    },
    selectProduct: function(productSequencenNumber)
    {
        this
        .waitForElementVisible('@menuTitle',timeOut)
        .clickNthelement('@menuTitle',productSequencenNumber);
    },
    }] 
}