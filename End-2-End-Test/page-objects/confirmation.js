module.exports = 
{
    elements: 
    {
      cardholderName: 
      {
        selector: '#cardholderName'
      },
      paymentCardNumber: 
      {
        selector: '#paymentCardNumber'
      },
      securityCode: 
      {
        selector: '#securityCode'
      },
      expiry: 
      {
        selector: '#expiry'
      },
      submitBtn: 
      {
        selector: '#submit'
      }
    },
    commands: [{
    validateConfirmationPage: function () 
    {
        this
        .api.useXpath()
        .waitForElementNotPresent(spinner, timeOut)
        .waitForElementVisible(currentMenu, timeOut)
        .expect.element(currentMenu).text.to.contain(stepFiveTitle);
        return this;
      },
      fillCardDetailsAndSubmit: function(details)
    {
        this
        .waitForElementVisible('@cardholderName',timeOut)
        .setValue('@cardholderName',details.cardholderName)
        .setValue('@paymentCardNumber',details.paymentCardNumber)
        .setValue('@securityCode',details.securityCode)
        .setValue('@expiry',details.expiry)
        .click('@submitBtn')
    },
    }] 
}