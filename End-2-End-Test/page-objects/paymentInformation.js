module.exports = 
{
    elements: 
    {
      submitBtn: 
      {
        selector: '#submit'
      }
    },
    commands: [{
    validatePaymentInformationPage: function () 
    {
        this
        .api.useXpath()
        .waitForElementNotPresent(spinner, timeOut)
        .waitForElementVisible(currentMenu, timeOut)
        .expect.element(currentMenu).text.to.contain(stepFourTitle);
        return this;
      },
      clickSubmit: function()
    {
        this
        .waitForElementVisible('@submitBtn',timeOut)  
        .click('@submitBtn');
    },
    }] 
}